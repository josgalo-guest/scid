Source: scid
Section: games
Priority: optional
Maintainer: Jose G. López <josgalo@jglopez.name>
Build-Depends: debhelper-compat (= 13), tcl-dev, tk-dev, zlib1g-dev, libx11-dev, dpkg-dev (>= 1.16.0), dh-python, python3
Standards-Version: 4.6.1
Homepage: http://scid.sf.net
Vcs-Git: https://salsa.debian.org/josgalo-guest/scid.git
Vcs-Browser: https://salsa.debian.org/josgalo-guest/scid

Package: scid
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, scid-data (= ${source:Version}), ${python3:Depends}, tcl, tk, zlib1g
Recommends: libsnack2, texlive-games, tdom, libtk-img, tcllib
Suggests: toga2, phalanx (>=22+d051004-9), glaurung, stockfish, crafty, scid-spell-data | scid-rating-data
Description: chess database with play and training functionality
 Shane's Chess Information Database is a chess database application with
 a graphical user interface. You can browse databases of chess games,
 edit them and search for games by various criteria. Scid uses its
 own compact and fast database format, but can convert to and from PGN.
 Its also possible to play live on the internet and practice tactical
 knowledge.
 .
 This package contains the main program.

Package: scid-data
Architecture: all
Depends: ${misc:Depends}, tclsh
Multi-Arch: foreign
Description: data files for scid, the chess database application
 Shane's Chess Information Database is a chess database application with
 a graphical user interface. You can browse databases of chess games,
 edit them and search for games by various criteria. Scid uses its
 own compact and fast database format, but can convert to and from PGN.
 Its also possible to play live on the internet and practice tactical
 knowledge.
 .
 This package contains scid data files.
